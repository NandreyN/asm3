#include "stdafx.h"
#include "CppUnitTest.h"
#include "../ASSEMBLER_3/Task1.cpp"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Task1Tests
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(Task1Method1)
		{
			vector<int> a = {1,1,1,1,1,2};
			ReturnData d = {5,0}, res= LongestSequence(a.data(),a.size());
			Assert::AreEqual(d.index, res.index);
			Assert::AreEqual(d.length, res.length);
		}

		TEST_METHOD(Task1Method2)
		{
			vector<int> a = {2,5,4,7,7,8,9,10,10 };
			ReturnData d = { 2,3 }, res = LongestSequence(a.data(), a.size());
			Assert::AreEqual(d.index, res.index);
			Assert::AreEqual(d.length, res.length);
		}
		TEST_METHOD(Task1Method3)
		{
			vector<int> a = { 4,4,4,4,4};
			ReturnData d = { 5,0 }, res = LongestSequence(a.data(), a.size());
			Assert::AreEqual(d.index, res.index);
			Assert::AreEqual(d.length, res.length);
		}

		TEST_METHOD(Task1Method4)
		{
			vector<int> a = { 1,2,3,4,5,6,7,8,9};
			ReturnData d = { 1,0 }, res = LongestSequence(a.data(), a.size());
			Assert::AreEqual(d.index, res.index);
			Assert::AreEqual(d.length, res.length);
		}
		TEST_METHOD(Task1Method5)
		{
			vector<int> a = { 8,7,5,5,5,5,1,2,2,2,2,2,2,2,2,2,2};
			ReturnData d = { 10,7 }, res = LongestSequence(a.data(), a.size());
			Assert::AreEqual(d.index, res.index);
			Assert::AreEqual(d.length, res.length);
		}

		TEST_METHOD(Task1Method6)
		{
			vector<int> a = {5};
			ReturnData d = { 1,0 }, res = LongestSequence(a.data(), a.size());
			Assert::AreEqual(d.index, res.index);
			Assert::AreEqual(d.length, res.length);
		}

		TEST_METHOD(Task1Method7)
		{
			vector<int> a = { 8,5,4,4,2,3,4,4,4,8,7,7,7};
			ReturnData d = { 3,6 }, res = LongestSequence(a.data(), a.size());
			Assert::AreEqual(d.index, res.index);
			Assert::AreEqual(d.length, res.length);
		}
	};
}