/*
4. ������������ ������ ��� ������������� ���������� ���������
(���� �������� �������� ����� 1, �� ������� ����������� ���������, ����� �� �����������), 
����� �����������, ����������� � �������� ���� ��������, �������� � ���� ��������. 
*/

#include <iostream>
#include <vector>
using namespace std;

void print(int* a, int l, int* data)
{
	cout << "{ ";
	int counter = 0;
	for (int i = 0; i < l; i++)
		if (a[i]) {
			cout << data[i] << " ";
			counter++;
		}
	if (!counter) cout << " empty ";
	cout << "}";
}

void Handling(int* arr1, int* arr2, int len, int* dataInDigits) // are considered to be the same length
{
	int *intersaction = new int[len];
	int *_union = new int[len];
	int *difference = new int[len];
	//for (int i = 0; i < len; i++)
		//res[i] = 0;

	_asm
	{
		mov ecx, len
		mov esi, 0
		mov  eax, [intersaction]
		mov ebx , [_union]
		mov edx, [difference]
		zero:
		mov int ptr [eax + esi*4], 0
		mov int ptr [ebx + esi * 4 ], 0
		mov int ptr[edx + esi * 4], 0
		inc esi
		loop zero

		mov ecx, len
		mov esi , 0
		cycle:
		mov eax,  [arr1]
		mov ebx , [arr2]
		mov eax, [eax + esi * 4]
		mov ebx , [ebx + esi*4]
		push eax
		//push ebx
		and eax , ebx
		je un
		mov eax , intersaction
		mov [eax + esi * 4], 1
		
		un:
		pop eax
		push eax
		//pop ebx
		or eax , ebx
		je diff
		mov eax , _union
		mov [eax + esi * 4], 1

		diff:
		pop eax
		sub eax ,ebx
		cmp eax, 0
		jle cnt
		mov eax , difference
		mov [eax + esi *4],1
		cnt : 
		inc esi
		loop cycle
	}
	cout << "_______________________________" << endl;
	cout << "A:"; print(arr1, len,dataInDigits); cout << endl << "B:"; print(arr2,len, dataInDigits);
	cout <<endl << "Intersaction : ";
	print(intersaction, len, dataInDigits);

	cout << endl << "Union: ";
	print(_union, len, dataInDigits);

	cout << endl << "A\\B: ";
	print(difference, len, dataInDigits);
	cout << endl;
	cout << "_______________________________" << endl;
	
	delete[] intersaction;
	delete[] _union;
}

int main()
{
	int a[] = {0,0,0}, b[] = {1,0,1};
	int d[] = {11,22,2};
	Handling(a, b, 3,d);

	int d1[] = {1,2,3,4};
	int a1[] = { 0,0,1,1}, b1[] = { 1,0,0,1};
	Handling(a1, b1, 4,d1);

	int d2[] = {99,33};
	int a2[] = { 1,0}, b2[] = { 0,1 };
	Handling(a2, b2, 2,d2);

	int d3[] = {5};
	int a3[] = { 0}, b3[] = { 1 };
	Handling(a3, b3, 1, d3);

	int d4[] = {44,6,3,7};
	int a4[] = { 1,0,0,1,0 }, b4[] = { 1,1,1,0,0};
	Handling(a4, b4, 5,d4);

	int d6[] = {1,334,67,899,12,3,55};
	int a6[] = {1,1,1,0,0,0,1};
	int b6[] = {1,1,0,0,1,0,1};
	Handling(a6, b6, 6, d6);
	return 0;
}