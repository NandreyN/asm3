/*
2. ����� ���������� ������ ������������ �����������.
����� ����� � ������� ������ ������ ���������, ����������� ���������� ����� ���������� 
��������� ���� �� ������ ���������.
���������, ��� ����� ���������� ����� ���� ���������.
*/

#include <iostream>
#include <vector>
using namespace std;

struct ReturnData
{
	int length;
	int index;
	
};

bool operator== (const ReturnData d1, const ReturnData d2)
{
	return (d2.length == d1.length && d2.index == d1.index) ? true : false;
}
ReturnData LongestSequence(int *arr, int len)
{
	ReturnData data = {1,0};
	
	int i = 1, indexLast = 0;
	// 0 1 44 44 44 8 55 55 55 55 2 03 3
	_asm
	{
		mov ecx, len
		dec ecx
		mov ebx, arr
		mov esi , 0
		jne enumeration
		mov data.length , 1
		mov data.index , 0
		jmp exit_asm
		cmp ecx , 0


		enumeration:
		mov eax, [ebx + esi * 4]
		inc esi
		mov edx, [ebx + esi * 4]
		
		cmp eax , edx
		jne not_eq
		inc i
		jmp cnt
		not_eq :
		mov eax, esi
		sub eax, i
		
		mov edx , i
		cmp edx, data.length
		jle refresh
		mov data.length , edx
		mov data.index , eax
		refresh : mov i , 1
		
			cnt:
		loop enumeration

			mov eax, esi
			sub eax, i
			inc eax

			mov edx, i
			cmp edx, data.length
			jle exit_asm
			mov data.length, edx
			mov data.index, eax
		exit_asm:
	}
	return data;
}

int main()
{
	return 0;
}